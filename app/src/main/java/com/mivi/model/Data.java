package com.mivi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("attributes")
    @Expose
    private Attributes_data attributes;
    @SerializedName("links")
    @Expose
    private Links_self links;
    @SerializedName("relationships")
    @Expose
    private Relationships_services relationships;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Attributes_data getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes_data attributes) {
        this.attributes = attributes;
    }

    public Links_self getLinks() {
        return links;
    }

    public void setLinks(Links_self links) {
        this.links = links;
    }

    public Relationships_services getRelationships() {
        return relationships;
    }

    public void setRelationships(Relationships_services relationships) {
        this.relationships = relationships;
    }

}
