package com.mivi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Services {

    @SerializedName("links")
    @Expose
    private Links_related links;

    public Links_related getLinks() {
        return links;
    }

    public void setLinks(Links_related links) {
        this.links = links;
    }

}
