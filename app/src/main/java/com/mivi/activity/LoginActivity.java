package com.mivi.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mivi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();


    @BindView(R.id.et_email)
    TextInputEditText et_email;

    @BindView(R.id.et_password)
    TextInputEditText et_password;

    @BindView(R.id.txt_login)
    TextView txt_login;


    @BindView(R.id.linlay_main)
    LinearLayout linlay_main;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        init();


        txt_login.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValidEmailId())
                    login();
                else
                    Toast.makeText(LoginActivity.this, R.string.error_invalid_email, Toast.LENGTH_SHORT).show();
            }
        });


    }

    private boolean isValidEmailId() {

        Log.d(TAG, et_email.getText().toString());

        return et_email.getText().toString().contains("@");
    }


    /**
     * init all variable
     */
    private void init() {


    }

    /**
     * all login business goes here
     */

    private void login() {

        //show login progress
        //  mProgressView.setVisibility(View.VISIBLE);


        final ProgressDialog progressdialog = new ProgressDialog(this);
        progressdialog.setMessage("Please Wait....");
        progressdialog.show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                progressdialog.dismiss();
                Intent i = new Intent(LoginActivity.this, CustomerDetailActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, 3000);

    }
}

