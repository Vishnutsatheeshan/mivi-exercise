package com.mivi.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.mivi.R;

import java.io.IOException;
import java.io.InputStream;

import com.mivi.adaptor.CustomerDetailAdaptor;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.mivi.model.Attributes_data;
import com.mivi.model.Collection;

/**
 * her we will show all details of customer in list
 */
public class CustomerDetailActivity extends AppCompatActivity {


    private static final String TAG = CustomerDetailActivity.class.getSimpleName();


    private CustomerDetailAdaptor mCustomerDetailAdaptor;


    @BindView(R.id.customer_detail_activity_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.txt_name)
    TextView txt_name;

    @BindView(R.id.txt_phone)
    TextView txt_phone;

    @BindView(R.id.txt_email)
    TextView txt_email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_customer_detail);
        ButterKnife.bind(this);
        Collection collection = parseJson(loadJSONFromAsset());

        mCustomerDetailAdaptor = new CustomerDetailAdaptor(this, collection.getIncluded());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mCustomerDetailAdaptor);
        setUserDetails(collection);

    }

    private void setUserDetails(Collection collection) {
        Attributes_data data = collection.getData().getAttributes();
        txt_name.setText(data.getTitle() + ". " + data.getFirstName() + " " + data.getLastName());

        txt_email.setText("Email: " + data.getEmailAddress());
        txt_phone.setText("Contact: " + data.getContactNumber());
    }

    private Collection parseJson(String json) {

        Gson gson = new Gson();

        Collection collection = gson.fromJson(json, Collection.class);

        return collection;
    }


    /**
     * This method is responsible for fetching the data from json which is stored in assets folder.
     */
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = this.getAssets().open("collections.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
