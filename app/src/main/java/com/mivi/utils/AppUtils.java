package utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by laltukumar on 07/05/18.
 */

public class AppUtils {
    /**
     * check internet connectivity
     *
     * @param context
     * @return
     */
    public static boolean isNetWorkAvailable(Context context) {

        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            if (connMgr == null) {
                return false;
            } else if (connMgr.getActiveNetworkInfo() != null && connMgr.getActiveNetworkInfo().isAvailable() && connMgr.getActiveNetworkInfo().isConnected()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }
}