package com.mivi.adaptor;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mivi.R;

import java.util.List;

import com.mivi.model.Included;

public class CustomerDetailAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Included> mList;


    public CustomerDetailAdaptor(Context mContext, List<Included> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_detail_item, parent, false);

        return new CustomerHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        CustomerHolder customerHolder =(CustomerHolder)holder;

        Included item= mList.get(position);

        customerHolder.name.setText("Name: "+item.getAttributes().getName());
        customerHolder.price.setText("Price: "+String.valueOf(item.getAttributes().getPrice()));
        customerHolder.type.setText(item.getType());
        customerHolder.id.setText(item.getId());

        if(item.getType().equalsIgnoreCase("products")){
            customerHolder.name.setVisibility(View.VISIBLE);
            customerHolder.price.setVisibility(View.VISIBLE);
        }else{
            customerHolder.name.setVisibility(View.GONE);
            customerHolder.price.setVisibility(View.GONE);
        }

        if(item.getType().equalsIgnoreCase("subscriptions")){
            customerHolder.name.setText("Data Balance: "+String.valueOf(item.getAttributes().getIncludedDataBalance()));
            customerHolder.name.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public int getItemCount() {

         if(mList==null)
             return 0;
         else
             return mList.size();
    }


    private class CustomerHolder extends RecyclerView.ViewHolder{

        public TextView type;
        public TextView id;
        public TextView name;
        public TextView price;

        public CustomerHolder(View itemView) {
            super(itemView);

            type=(TextView)itemView.findViewById(R.id.customer_detail_item_tv_type);
            id=(TextView)itemView.findViewById(R.id.customer_detail_item_tv_id);
            name = (TextView)itemView.findViewById(R.id.customer_detail_item_tv_name);
            price = (TextView)itemView.findViewById(R.id.customer_detail_item_tv_price);
        }
    }

}
